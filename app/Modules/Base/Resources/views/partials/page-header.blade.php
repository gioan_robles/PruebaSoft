<div class="page-header">
    <!-- BEGIN HEADER TOP -->
    <div class="page-header-top">
        <div class="container-fluid">
            <!-- BEGIN LOGO -->
            <!-- END LOGO -->
            <!-- BEGIN RESPONSIVE MENU TOGGLER -->
            <a class="menu-toggler" href="javascript:;">
            </a>
            <!-- END RESPONSIVE MENU TOGGLER -->
            <!-- BEGIN TOP NAVIGATION MENU -->
            <div class="top-menu">
                <ul class="nav navbar-nav pull-right">
                    <!-- BEGIN USER LOGIN DROPDOWN -->
                    <li class="dropdown dropdown-user dropdown-dark">
                        <a class="dropdown-toggle" data-close-others="true" data-hover="dropdown" data-toggle="dropdown" href="javascript:;">
                            {{-- @if(is_file('public/img/usuarios/' . $usuario->personas->foto ))
                            <img alt="" class="img-circle" src="{{ url('img/usuarios/' . $usuario->personas->foto) }}">
                                @else
                                <img alt="" class="img-circle" src="{{ url('img/usuarios/logo.png') }}">
                                    @endif --}}
                                    <span class="username username-hide-mobile">
                                        {{ $usuario->personas->nombres }}
                                    </span>
                                </img>
                            </img>
                        </a>
                        @if($usuario->id > 0)
                        <ul class="dropdown-menu dropdown-menu-default">
                            <li>
                                <a href="{{ url(Config::get('admin.prefix').'/perfil') }}">
                                    <i class="fa fa-user">
                                    </i>
                                    Mi Perfil
                                </a>
                            </li>
                            <li class="divider">
                            </li>
                            <li>
                                <a href="{{ url(Config::get('admin.prefix').'/login/salir') }}">
                                    <i class="icon-logout">
                                    </i>
                                    Salir
                                </a>
                            </li>
                        </ul>
                        @endif
                    </li>
                    <!-- END USER LOGIN DROPDOWN -->
                    <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                    <li class="">
                        <a href="{{ url(Config::get('admin.prefix').'/login/salir') }}">
                            <span class="sr-only">
                                Salir
                            </span>
                            <i class="icon-logout">
                            </i>
                        </a>
                    </li>
                    <!-- END QUICK SIDEBAR TOGGLER -->
                </ul>
            </div>
            <!-- END TOP NAVIGATION MENU -->
        </div>
    </div>
    <!-- END HEADER TOP -->
    @include('base::partials.menu')
</div>
