<?php

namespace App\Modules\Noticias\Http\Requests;

use App\Http\Requests\Request;

class EtiquetasRequest extends Request {
    protected $reglasArr = [
		'nombre' => ['required', 'min:3', 'max:250'], 
		'slug' => ['required', 'min:3', 'max:250', 'unique:etiquetas,slug']
	];
}